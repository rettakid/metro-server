package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class UserBodyFatDto extends BaseDto {

    private UserDto user;
    private Float bodyFat;

    @NotNull
    @Valid
    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    @NotNull
    public Float getBodyFat() {
        return bodyFat;
    }

    public void setBodyFat(Float bodyFat) {
        this.bodyFat = bodyFat;
    }
}
