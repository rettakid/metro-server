package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class WorkoutCycleDto extends BaseDto {

    private WorkoutDto workout;
    private CycleDto cycle;

    @Valid
    @NotNull
    public WorkoutDto getWorkout() {
        return workout;
    }

    public void setWorkout(WorkoutDto workout) {
        this.workout = workout;
    }

    @Valid
    @NotNull
    public CycleDto getCycle() {
        return cycle;
    }

    public void setCycle(CycleDto cycle) {
        this.cycle = cycle;
    }
}
