package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;
import za.co.lwethu.vitruvianphysique.common.enums.LiftTypeEnum;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ProgramExerciseSelectionDto extends BaseDto {

    private UserProgramDto userProgram;
    private ExerciseTypeDto exerciseType;

    @NotNull
    @Valid
    public UserProgramDto getUserProgram() {
        return userProgram;
    }

    public void setUserProgram(UserProgramDto userProgram) {
        this.userProgram = userProgram;
    }

    @NotNull
    @Valid
    public ExerciseTypeDto getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseTypeDto exerciseType) {
        this.exerciseType = exerciseType;
    }

}
