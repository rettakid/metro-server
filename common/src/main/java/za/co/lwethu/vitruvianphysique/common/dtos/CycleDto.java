package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CycleDto extends BaseDto {

    private String name;
    private Integer sequenceNumber;
    private CycleDto cycle;
    private List<CycleDto> cycles;

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @Valid
    public CycleDto getCycle() {
        return cycle;
    }

    public void setCycle(CycleDto cycle) {
        this.cycle = cycle;
    }

    public List<CycleDto> getCycles() {
        return cycles;
    }

    public void setCycles(List<CycleDto> cycles) {
        this.cycles = cycles;
    }
}
