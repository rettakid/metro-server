package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ExerciseTypeDto extends BaseDto {

    private String name;
    private String description;
    private Integer bodyWeighImpact;
    private Boolean requiresEquipment;
    private Boolean requiresWeights;
    private Boolean isometric;
    private Boolean timeBased;
    private Integer concentricPeriod;
    private Integer concentricHoldPeriod;
    private Integer eccentricPeriod;
    private Integer eccentricHoldPeriod;
    private ImageFileDto image;
    private VideoFileDto video;
    private List<ExerciseMuscleGroupImpactDto> muscleGroupImpacts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getBodyWeighImpact() {
        return bodyWeighImpact;
    }

    public void setBodyWeighImpact(Integer bodyWeighImpact) {
        this.bodyWeighImpact = bodyWeighImpact;
    }

    public Boolean getRequiresEquipment() {
        return requiresEquipment;
    }

    public void setRequiresEquipment(Boolean requiresEquipment) {
        this.requiresEquipment = requiresEquipment;
    }

    public Boolean getRequiresWeights() {
        return requiresWeights;
    }

    public void setRequiresWeights(Boolean requiresWeights) {
        this.requiresWeights = requiresWeights;
    }

    public Boolean getIsometric() {
        return isometric;
    }

    public void setIsometric(Boolean isometric) {
        this.isometric = isometric;
    }

    public Boolean getTimeBased() {
        return timeBased;
    }

    public void setTimeBased(Boolean timeBased) {
        this.timeBased = timeBased;
    }

    public Integer getConcentricPeriod() {
        return concentricPeriod;
    }

    public void setConcentricPeriod(Integer concentricPeriod) {
        this.concentricPeriod = concentricPeriod;
    }

    public Integer getConcentricHoldPeriod() {
        return concentricHoldPeriod;
    }

    public void setConcentricHoldPeriod(Integer concentricHoldPeriod) {
        this.concentricHoldPeriod = concentricHoldPeriod;
    }

    public Integer getEccentricPeriod() {
        return eccentricPeriod;
    }

    public void setEccentricPeriod(Integer eccentricPeriod) {
        this.eccentricPeriod = eccentricPeriod;
    }

    public Integer getEccentricHoldPeriod() {
        return eccentricHoldPeriod;
    }

    public void setEccentricHoldPeriod(Integer eccentricHoldPeriod) {
        this.eccentricHoldPeriod = eccentricHoldPeriod;
    }

    public ImageFileDto getImage() {
        return image;
    }

    public void setImage(ImageFileDto image) {
        this.image = image;
    }

    public VideoFileDto getVideo() {
        return video;
    }

    public void setVideo(VideoFileDto video) {
        this.video = video;
    }

    public List<ExerciseMuscleGroupImpactDto> getMuscleGroupImpacts() {
        return muscleGroupImpacts;
    }

    public void setMuscleGroupImpacts(List<ExerciseMuscleGroupImpactDto> muscleGroupImpacts) {
        this.muscleGroupImpacts = muscleGroupImpacts;
    }
}
