package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ImageFileDto extends BaseDto {

    private FileObjectDto file;

    @NotNull
    @Valid
    public FileObjectDto getFile() {
        return file;
    }

    public void setFile(FileObjectDto file) {
        this.file = file;
    }

}
