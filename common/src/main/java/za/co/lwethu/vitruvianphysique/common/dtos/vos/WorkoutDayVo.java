package za.co.lwethu.vitruvianphysique.common.dtos.vos;

import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutDto;

import java.util.Date;
import java.util.List;

public class WorkoutDayVo {

    private Date date;
    private List<WorkoutDto> completedWorkouts;
    private List<WorkoutDto> expectedWorkouts;

    public WorkoutDayVo() {
    }


    public WorkoutDayVo(Date date, List<WorkoutDto> completedWorkouts, List<WorkoutDto> expectedWorkouts) {
        this.date = date;
        this.completedWorkouts = completedWorkouts;
        this.expectedWorkouts = expectedWorkouts;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<WorkoutDto> getCompletedWorkouts() {
        return completedWorkouts;
    }

    public void setCompletedWorkouts(List<WorkoutDto> completedWorkouts) {
        this.completedWorkouts = completedWorkouts;
    }

    public List<WorkoutDto> getExpectedWorkouts() {
        return expectedWorkouts;
    }

    public void setExpectedWorkouts(List<WorkoutDto> expectedWorkouts) {
        this.expectedWorkouts = expectedWorkouts;
    }
}
