package za.co.lwethu.vitruvianphysique.common.dtos;

public class ExerciseTemplateDto {

    private ProgramDto program;
    private ExerciseTypeDto exerciseType;
    private MuscleGroupDto muscleGroup;
    private Boolean randomise;

    public ProgramDto getProgram() {
        return program;
    }

    public void setProgram(ProgramDto program) {
        this.program = program;
    }

    public ExerciseTypeDto getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseTypeDto exerciseType) {
        this.exerciseType = exerciseType;
    }

    public MuscleGroupDto getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(MuscleGroupDto muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    public Boolean getRandomise() {
        return randomise;
    }

    public void setRandomise(Boolean randomise) {
        this.randomise = randomise;
    }
}
