package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;
import za.co.lwethu.vitruvianphysique.common.enums.DifficultyEnum;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ExerciseDto extends BaseDto {

    private Integer sequenceNumber;
    private WorkoutExerciseDto workoutExercise;
    private ExerciseTemplateDto exerciseTemplate;
    private Integer restPeriod;
    private ExerciseTypeDto type;
    private List<ExerciseSetDto> sets;

    @NotNull
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public WorkoutExerciseDto getWorkoutExercise() {
        return workoutExercise;
    }

    public void setWorkoutExercise(WorkoutExerciseDto workoutExercise) {
        this.workoutExercise = workoutExercise;
    }

    public ExerciseTemplateDto getExerciseTemplate() {
        return exerciseTemplate;
    }

    public void setExerciseTemplate(ExerciseTemplateDto exerciseTemplate) {
        this.exerciseTemplate = exerciseTemplate;
    }

    public Integer getRestPeriod() {
        return restPeriod;
    }

    public void setRestPeriod(Integer restPeriod) {
        this.restPeriod = restPeriod;
    }

    @Valid
    public ExerciseTypeDto getType() {
        return type;
    }

    public void setType(ExerciseTypeDto type) {
        this.type = type;
    }

    public List<ExerciseSetDto> getSets() {
        return sets;
    }

    public void setSets(List<ExerciseSetDto> sets) {
        this.sets = sets;
    }
}
