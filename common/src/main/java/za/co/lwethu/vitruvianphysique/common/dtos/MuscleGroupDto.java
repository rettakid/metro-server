package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.constraints.NotNull;

public class MuscleGroupDto extends BaseDto {

    private String name;
    private String description;
    private ImageFileDto image;

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ImageFileDto getImage() {
        return image;
    }

    public void setImage(ImageFileDto image) {
        this.image = image;
    }
}
