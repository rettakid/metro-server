package za.co.lwethu.vitruvianphysique.common.enums;

public enum DifficultyEnum {

    TO_EASY("That was a joke"),
    EASY("Felt something"),
    EXCELLENT("Great burn, Didn't pause"),
    HARD("Amazing Burn, Had to pause, kept form"),
    TO_HARD("Could not keep form");

    private String description;

    DifficultyEnum(String description) {
        this.description = description;
    }
}
