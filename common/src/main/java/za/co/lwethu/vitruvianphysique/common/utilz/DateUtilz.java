package za.co.lwethu.vitruvianphysique.common.utilz;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilz {

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final Date END_OF_TIME;
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_TIME_FORMAT);


    static {
        END_OF_TIME = getDate("9999-12-31 23:59:59");
    }

    private static Date getDate(String format) {
        Date date;
        try {
            date = SIMPLE_DATE_FORMAT.parse(format);
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }

}
