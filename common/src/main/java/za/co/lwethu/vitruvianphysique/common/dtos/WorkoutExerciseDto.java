package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import java.util.List;

public class WorkoutExerciseDto extends BaseDto {

    private WorkoutDto workout;
    private Integer restPeriod;
    private List<ExerciseDto> exercises;

    public WorkoutDto getWorkout() {
        return workout;
    }

    public void setWorkout(WorkoutDto workout) {
        this.workout = workout;
    }

    public Integer getRestPeriod() {
        return restPeriod;
    }

    public void setRestPeriod(Integer restPeriod) {
        this.restPeriod = restPeriod;
    }

    public List<ExerciseDto> getExercises() {
        return exercises;
    }

    public void setExercises(List<ExerciseDto> exercises) {
        this.exercises = exercises;
    }
}
