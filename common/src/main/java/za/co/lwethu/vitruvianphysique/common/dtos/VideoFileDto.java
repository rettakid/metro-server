package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class VideoFileDto extends BaseDto {

    private FileObjectDto file;
    private FileObjectDto thumbnail;

    @NotNull
    @Valid
    public FileObjectDto getFile() {
        return file;
    }

    public void setFile(FileObjectDto file) {
        this.file = file;
    }

    @NotNull
    @Valid
    public FileObjectDto getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(FileObjectDto thumbnail) {
        this.thumbnail = thumbnail;
    }
}
