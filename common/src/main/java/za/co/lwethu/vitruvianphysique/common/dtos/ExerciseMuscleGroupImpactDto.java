package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

public class ExerciseMuscleGroupImpactDto extends BaseDto {

    private ExerciseTypeDto exerciseType;
    private MuscleGroupDto muscleGroup;
    private Boolean symmetrical;
    private Integer impact;

    public ExerciseTypeDto getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseTypeDto exerciseType) {
        this.exerciseType = exerciseType;
    }

    public MuscleGroupDto getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(MuscleGroupDto muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    public Boolean getSymmetrical() {
        return symmetrical;
    }

    public void setSymmetrical(Boolean symmetrical) {
        this.symmetrical = symmetrical;
    }

    public Integer getImpact() {
        return impact;
    }

    public void setImpact(Integer impact) {
        this.impact = impact;
    }
}
