package za.co.lwethu.vitruvianphysique.common.dtos.vos;

import za.co.lwethu.vitruvianphysique.common.dtos.ProgramExerciseSelectionDto;
import za.co.lwethu.vitruvianphysique.common.dtos.UserOneRepMaxDto;
import za.co.lwethu.vitruvianphysique.common.dtos.UserProgramDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class UserProgramCreationVo {

    private UserProgramDto userProgram;
    private List<ProgramExerciseSelectionDto> exerciseSelection;
    private List<UserOneRepMaxDto> oneRepMaxxes;

    @NotNull
    @Valid
    public UserProgramDto getUserProgram() {
        return userProgram;
    }

    public void setUserProgram(UserProgramDto userProgram) {
        this.userProgram = userProgram;
    }

    @NotNull
    @Valid
    public List<ProgramExerciseSelectionDto> getExerciseSelection() {
        return exerciseSelection;
    }

    public void setExerciseSelection(List<ProgramExerciseSelectionDto> exerciseSelection) {
        this.exerciseSelection = exerciseSelection;
    }

    @NotNull
    @Valid
    public List<UserOneRepMaxDto> getOneRepMaxxes() {
        return oneRepMaxxes;
    }

    public void setOneRepMaxxes(List<UserOneRepMaxDto> oneRepMaxxes) {
        this.oneRepMaxxes = oneRepMaxxes;
    }

}
