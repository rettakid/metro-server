package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class UserProgramDto extends BaseDto {

    private UserDto user;
    private ProgramDto program;

    @NotNull
    @Valid
    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    @NotNull
    @Valid
    public ProgramDto getProgram() {
        return program;
    }

    public void setProgram(ProgramDto program) {
        this.program = program;
    }

}
