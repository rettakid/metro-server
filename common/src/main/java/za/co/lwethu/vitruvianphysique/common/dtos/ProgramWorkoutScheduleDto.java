package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ProgramWorkoutScheduleDto extends BaseDto {

    private ProgramDto program;
    private CycleDto cycle;

    @NotNull
    @Valid
    public ProgramDto getProgram() {
        return program;
    }

    public void setProgram(ProgramDto program) {
        this.program = program;
    }

    @NotNull
    @Valid
    public CycleDto getCycle() {
        return cycle;
    }

    public void setCycle(CycleDto cycle) {
        this.cycle = cycle;
    }

}
