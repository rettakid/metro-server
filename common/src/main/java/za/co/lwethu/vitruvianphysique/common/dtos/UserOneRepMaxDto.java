package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class UserOneRepMaxDto extends BaseDto {

    private UserDto user;
    private ExerciseTypeDto exerciseType;
    private Float weight;
    private Integer reps;
    private Float oneRepMax;

    @NotNull
    @Valid
    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    @NotNull
    @Valid
    public ExerciseTypeDto getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseTypeDto exerciseType) {
        this.exerciseType = exerciseType;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    @NotNull
    public Float getOneRepMax() {
        return oneRepMax;
    }

    public void setOneRepMax(Float oneRepMax) {
        this.oneRepMax = oneRepMax;
    }
}
