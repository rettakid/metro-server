package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;
import za.co.lwethu.vitruvianphysique.common.enums.DifficultyEnum;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ExerciseSetDto extends BaseDto {

    private ExerciseDto exercise;
    private Integer sequenceNumber;
    private Integer restPeriod;
    private Double weight;
    private Integer reps;
    private Boolean toFailure;
    private DifficultyEnum difficulty;

    public ExerciseDto getExercise() {
        return exercise;
    }

    public void setExercise(ExerciseDto exercise) {
        this.exercise = exercise;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Integer getRestPeriod() {
        return restPeriod;
    }

    public void setRestPeriod(Integer restPeriod) {
        this.restPeriod = restPeriod;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    public Boolean getToFailure() {
        return toFailure;
    }

    public void setToFailure(Boolean toFailure) {
        this.toFailure = toFailure;
    }

    public DifficultyEnum getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(DifficultyEnum difficulty) {
        this.difficulty = difficulty;
    }
}
