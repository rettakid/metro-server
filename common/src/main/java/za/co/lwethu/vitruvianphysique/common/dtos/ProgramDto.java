package za.co.lwethu.vitruvianphysique.common.dtos;

import za.co.lwethu.vitruvianphysique.common.dtos.type.BaseDto;

import javax.validation.constraints.NotNull;
import java.util.List;

public class ProgramDto extends BaseDto {

    private String name;
    private Boolean premium;
    private UserDto owner;
    private List<ProgramWorkoutScheduleDto> workoutSchedule;

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public Boolean getPremium() {
        return premium;
    }

    public void setPremium(Boolean premium) {
        this.premium = premium;
    }

    public UserDto getOwner() {
        return owner;
    }

    public void setOwner(UserDto owner) {
        this.owner = owner;
    }

    public List<ProgramWorkoutScheduleDto> getWorkoutSchedule() {
        return workoutSchedule;
    }

    public void setWorkoutSchedule(List<ProgramWorkoutScheduleDto> workoutSchedule) {
        this.workoutSchedule = workoutSchedule;
    }
}
