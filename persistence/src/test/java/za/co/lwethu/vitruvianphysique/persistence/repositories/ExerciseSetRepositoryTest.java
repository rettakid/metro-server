package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.common.enums.DifficultyEnum;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseSet;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ExerciseSetRepositoryTest {

    @Autowired
    private ExerciseSetRepository exerciseSetRepository;

    @Test
    public void findAll() {
        List<ExerciseSet> exerciseSets = exerciseSetRepository.findAll();
        assertEquals(3, exerciseSets.size());
    }

    @Test
    public void getOne() {
        ExerciseSet exerciseSet = exerciseSetRepository.getOne(1L);
        assertEquals(DifficultyEnum.EASY, exerciseSet.getDifficulty());
    }
}