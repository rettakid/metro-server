package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.Exercise;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ExerciseRepositoryTest {

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Test
    public void findAll() {
        List<Exercise> exercises = exerciseRepository.findAll();
        assertEquals(3, exercises.size());
    }

    @Test
    public void getOne() {
        Exercise exercise = exerciseRepository.getOne(2L);
        assertEquals(2, (int) exercise.getSequenceNumber());
    }

}