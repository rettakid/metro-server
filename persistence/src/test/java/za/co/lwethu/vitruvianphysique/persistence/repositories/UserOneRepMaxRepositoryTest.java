package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.UserOneRepMax;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserOneRepMaxRepositoryTest {

    @Autowired
    private UserOneRepMaxRepository userOneRepMaxRepository;

    @Test
    public void findALl() throws Exception {
        List<UserOneRepMax> userOneRepMaxes = userOneRepMaxRepository.findAll();
        assertEquals(3, userOneRepMaxes.size());
    }

    @Test
    public void getOne() throws Exception {
        UserOneRepMax userOneRepMax = userOneRepMaxRepository.getOne(1L);
        assertEquals(100f, userOneRepMax.getOneRepMax(), 0f);
    }

}