package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.Workout;
import za.co.lwethu.vitruvianphysique.persistence.specifications.WorkoutSpecification;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class WorkoutRepositoryTest {

    @Autowired
    private WorkoutRepository workoutRepository;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void findAll() {
        List<Workout> workouts = workoutRepository.findAll();
        assertEquals(3, workouts.size());
    }

    @Test
    public void getOne() {
        Workout workout = workoutRepository.getOne(1L);
        assertEquals("Heavy Bench",workout.getName());
    }

    @Test
    public void workoutBetween() throws ParseException {
        List<Workout> microCycleWorkouts = workoutRepository.findAll(
                WorkoutSpecification.workoutBetween(simpleDateFormat.parse("2018-01-01"), simpleDateFormat.parse("2018-03-01"))
        );
        assertEquals(2, microCycleWorkouts.size());
    }

}
