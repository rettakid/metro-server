package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.UserProgram;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserProgramRepositoryTest {

    @Autowired
    private UserProgramRepository userProgramRepository;

    @Test
    public void findAll() throws Exception {
        List<UserProgram> userPrograms = userProgramRepository.findAll();
        assertEquals(3, userPrograms.size());
    }

    @Test
    public void getOne() throws Exception {
        UserProgram userProgram = userProgramRepository.getOne(1L);
        assertEquals("Lwazi",userProgram.getUser().getName());
    }

}
