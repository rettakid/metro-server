package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.ProgramExerciseSelection;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProgramExerciseSelectionRepositoryTest {

    @Autowired
    private ProgramExerciseSelectionRepository programExerciseSelectionRepository;

    @Test
    public void findAll() throws Exception {
        List<ProgramExerciseSelection> exerciseSelections = programExerciseSelectionRepository.findAll();
        assertEquals(3, exerciseSelections.size());
    }

    @Test
    public void getOne() throws Exception {
        ProgramExerciseSelection programExerciseSelection = programExerciseSelectionRepository.getOne(1L);
        assertEquals("Push Ups", programExerciseSelection.getExerciseType().getName());
    }

}
