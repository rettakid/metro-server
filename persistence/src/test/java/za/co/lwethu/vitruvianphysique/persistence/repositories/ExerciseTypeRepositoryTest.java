package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseType;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ExerciseTypeRepositoryTest {

    @Autowired
    private ExerciseTypeRepository exerciseTypeRepository;

    @Test
    public void findAll() throws Exception {
        List<ExerciseType> exerciseTypes = exerciseTypeRepository.findAll();
        assertEquals(3, exerciseTypes.size());
    }

    @Test
    public void getOne() throws Exception {
        ExerciseType exerciseType = exerciseTypeRepository.getOne(1L);
        assertEquals("Push Ups",exerciseType.getName());
    }

}