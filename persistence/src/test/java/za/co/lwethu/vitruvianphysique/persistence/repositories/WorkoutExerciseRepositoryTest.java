package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.Workout;
import za.co.lwethu.vitruvianphysique.persistence.entities.WorkoutExercise;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class WorkoutExerciseRepositoryTest {

    @Autowired
    private WorkoutExerciseRepository workoutExerciseRepository;

    @Test
    public void findAll() {
        List<WorkoutExercise> workoutExercises = workoutExerciseRepository.findAll();
        assertEquals(3, workoutExercises.size());
    }

    @Test
    public void getOne() {
        WorkoutExercise workoutExercise = workoutExerciseRepository.getOne(1L);
        assertEquals(60, (int) workoutExercise.getRestPeriod());
    }
}