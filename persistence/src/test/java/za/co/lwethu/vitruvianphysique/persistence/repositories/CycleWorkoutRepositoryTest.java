package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.WorkoutCycle;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CycleWorkoutRepositoryTest {

    @Autowired
    private WorkoutCycleRepository workoutCycleRepository;

    @Test
    public void findAll() {
        List<WorkoutCycle> workoutCycles = workoutCycleRepository.findAll();
        assertEquals(3, workoutCycles.size());
    }

    @Test
    public void getOne() {
        WorkoutCycle workoutCycle = workoutCycleRepository.getOne(1L);
        assertEquals("Heavy Bench", workoutCycle.getWorkout().getName());
    }

}