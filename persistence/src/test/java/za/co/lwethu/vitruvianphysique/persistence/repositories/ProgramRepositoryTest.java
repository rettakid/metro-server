package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.Program;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProgramRepositoryTest {

    @Autowired
    private ProgramRepository programRepository;

    @Test
    public void findAll() throws Exception {
        List<Program> programs = programRepository.findAll();
        assertEquals(3, programs.size());
    }

    @Test
    public void getOne() throws Exception {
        Program program = programRepository.getOne(1L);
        assertEquals("Hybrid 5",program.getName());
    }
}