package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.MuscleGroup;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MuscleGroupRepositoryTest {

    @Autowired
    private MuscleGroupRepository muscleGroupRepository;

    @Test
    public void findAll() throws Exception {
        List<MuscleGroup> muscleGroups = muscleGroupRepository.findAll();
        assertEquals(3, muscleGroups.size());
    }

    @Test
    public void getOne() throws Exception {
        MuscleGroup muscleGroup = muscleGroupRepository.getOne(1L);
        assertEquals("Chest", muscleGroup.getName());
    }
}
