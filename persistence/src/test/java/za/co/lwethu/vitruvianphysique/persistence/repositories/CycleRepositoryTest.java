package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.lwethu.vitruvianphysique.persistence.entities.Cycle;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CycleRepositoryTest {

    @Autowired
    private CycleRepository cycleRepository;

    @Test
    public void findAll() {
        List<Cycle> cycles = cycleRepository.findAll();
        assertEquals(9, cycles.size());
    }

    @Test
    public void getOne() {
        Cycle cycle = cycleRepository.getOne(1L);
        assertEquals("Cycle 1", cycle.getName());
    }

}
