
insert into muscle_group(id, valid_from, valid_to, name, description) values (1, '2018-01-01', '9999-01-01','Chest','');
insert into muscle_group(id, valid_from, valid_to, name, description) values (2, '2018-01-01', '9999-01-01','Pull Compound','');
insert into muscle_group(id, valid_from, valid_to, name, description) values (3, '2018-01-01', '9999-01-01','Legs','');

insert into program(id, valid_from, valid_to, premium, name) values (1, '2018-01-01', '9999-01-01', true, 'Hybrid 5');
insert into program(id, valid_from, valid_to, premium, name) values (2, '2018-01-01', '9999-01-01', true, 'Hybrid 4');
insert into program(id, valid_from, valid_to, premium, name) values (3, '2018-01-01', '9999-01-01', true, 'Hybrid 3');

insert into cycle(id, valid_from, valid_to, name, sequence_number) values (1, '2018-01-01', '9999-01-01', 'Cycle 1', 1);
insert into cycle(id, valid_from, valid_to, name, sequence_number) values (2, '2018-01-01', '9999-01-01', 'Cycle 2', 2);
insert into cycle(id, valid_from, valid_to, name, sequence_number) values (3, '2018-01-01', '9999-01-01', 'Cycle 3', 3);

insert into cycle(id, valid_from, valid_to, name, sequence_number, cycle_id) values (4, '2018-01-01', '9999-01-01', 'Cycle 1', 1, 1);
insert into cycle(id, valid_from, valid_to, name, sequence_number, cycle_id) values (5, '2018-01-01', '9999-01-01', 'Cycle 2', 2, 1);
insert into cycle(id, valid_from, valid_to, name, sequence_number, cycle_id) values (6, '2018-01-01', '9999-01-01', 'Cycle 1', 1, 2);

insert into cycle(id, valid_from, valid_to, name, sequence_number, cycle_id) values (7, '2018-01-01', '9999-01-01', 'Round 1', 1, 1);
insert into cycle(id, valid_from, valid_to, name, sequence_number, cycle_id) values (8, '2018-01-01', '9999-01-01', 'Round 2', 2, 1);
insert into cycle(id, valid_from, valid_to, name, sequence_number, cycle_id) values (9, '2018-01-01', '9999-01-01', 'Round 3', 3, 1);

insert into workout(id, valid_from, valid_to, name, sequence_number, workout_date) values (1, '2018-01-01', '9999-01-01', 'Heavy Bench', 1, '2018-01-15');
insert into workout(id, valid_from, valid_to, name, sequence_number, workout_date) values (2, '2018-01-01', '9999-01-01', 'Light Pull', 2, '2018-02-15');
insert into workout(id, valid_from, valid_to, name, sequence_number, workout_date) values (3, '2018-01-01', '9999-01-01', 'Heavy Squat', 3, '2018-03-15');

insert into workout_cycle(id, valid_from, valid_to, cycle_id, workout_id) values (1, '2018-01-01', '9999-01-01', 1, 1);
insert into workout_cycle(id, valid_from, valid_to, cycle_id, workout_id) values (2, '2018-01-01', '9999-01-01', 1, 2);
insert into workout_cycle(id, valid_from, valid_to, cycle_id, workout_id) values (3, '2018-01-01', '9999-01-01', 1, 3);

insert into exercise_type(id, valid_from, valid_to, name, description, body_weigh_impact, requires_equipment, requires_weights, isometric, time_based, concentric_period, concentric_hold_period, eccentric_period, eccentric_hold_period, image_id, video_id) values (1, '2018-01-01', '9999-01-01', 'Push Ups', '', 50, false, false, false, false, 1, 0, 2, 0, null, null);
insert into exercise_type(id, valid_from, valid_to, name, description, body_weigh_impact, requires_equipment, requires_weights, isometric, time_based, concentric_period, concentric_hold_period, eccentric_period, eccentric_hold_period, image_id, video_id) values (2, '2018-01-01', '9999-01-01', 'Pull Ups', '', 80, true, false, false, false, 1, 0, 2, 0, null, null);
insert into exercise_type(id, valid_from, valid_to, name, description, body_weigh_impact, requires_equipment, requires_weights, isometric, time_based, concentric_period, concentric_hold_period, eccentric_period, eccentric_hold_period, image_id, video_id) values (3, '2018-01-01', '9999-01-01', 'Rows', '', 0, true, true, false, false, 1, 0, 2, 0, null, null);

insert into workout_exercise(id, valid_from, valid_to, workout_id, rest_period) values (1, '2018-01-01', '9999-01-01', 1, 60);
insert into workout_exercise(id, valid_from, valid_to, workout_id, rest_period) values (2, '2018-01-01', '9999-01-01', 1, 60);
insert into workout_exercise(id, valid_from, valid_to, workout_id, rest_period) values (3, '2018-01-01', '9999-01-01', 1, 60);

insert into exercise(id, valid_from, valid_to, sequence_number, workout_exercise_id, rest_period, type_id) values (1, '2018-01-01', '9999-01-01', 1, 1, 15, 1);
insert into exercise(id, valid_from, valid_to, sequence_number, workout_exercise_id, rest_period, type_id) values (2, '2018-01-01', '9999-01-01', 2, 1, 15, 2);
insert into exercise(id, valid_from, valid_to, sequence_number, workout_exercise_id, rest_period, type_id) values (3, '2018-01-01', '9999-01-01', 3, 1, 15, 3);

insert into exercise_set(id, valid_from, valid_to, exercise_id, sequence_number, rest_period, weight, reps, to_failure, difficulty) values (1, '2018-01-01', '9999-01-01', 1, 1, 15, 10, 12, false, 1);
insert into exercise_set(id, valid_from, valid_to, exercise_id, sequence_number, rest_period, weight, reps, to_failure, difficulty) values (2, '2018-01-01', '9999-01-01', 1, 2, 15, 10, 12, false, 1);
insert into exercise_set(id, valid_from, valid_to, exercise_id, sequence_number, rest_period, weight, reps, to_failure, difficulty) values (3, '2018-01-01', '9999-01-01', 1, 3, 15, 10, 12, false, 1);

-- user --

insert into user(id, valid_from, valid_to, email, password, name, surname, date_of_birth, image_id) values (1, '2018-01-01', '9999-01-01', 'lwazi.prusent@gmail.com', '12345', 'Lwazi', 'Prusent', '1989-02-13', null);
insert into user(id, valid_from, valid_to, email, password, name, surname, date_of_birth, image_id) values (2, '2018-01-01', '9999-01-01', 'lwazi2.prusent@gmail.com', '12345', 'Lwazi', 'Prusent', '1989-02-13', null);
insert into user(id, valid_from, valid_to, email, password, name, surname, date_of_birth, image_id) values (3, '2018-01-01', '9999-01-01', 'lwazi3.prusent@gmail.com', '12345', 'Lwazi', 'Prusent', '1989-02-13', null);

insert into user_program(id, valid_from, valid_to, user_id, program_id) values (1, '2018-01-01', '9999-01-01', 1, 1);
insert into user_program(id, valid_from, valid_to, user_id, program_id) values (2, '2018-01-01', '9999-01-01', 2, 2);
insert into user_program(id, valid_from, valid_to, user_id, program_id) values (3, '2018-01-01', '9999-01-01', 3, 3);

insert into program_exercise_selection(id, valid_from, valid_to, user_program_id, exercise_type_id) values (1, '2018-01-01', '9999-01-01', 1, 1);
insert into program_exercise_selection(id, valid_from, valid_to, user_program_id, exercise_type_id) values (2, '2018-01-01', '9999-01-01', 1, 1);
insert into program_exercise_selection(id, valid_from, valid_to, user_program_id, exercise_type_id) values (3, '2018-01-01', '9999-01-01', 1, 1);

insert into user_one_rep_max(id, valid_from, valid_to, user_id, exercise_type_id, weight, reps, one_rep_max) values (1, '2018-01-01', '9999-01-01', 1, 1, 80, 4, 100);
insert into user_one_rep_max(id, valid_from, valid_to, user_id, exercise_type_id, weight, reps, one_rep_max) values (2, '2018-01-01', '9999-01-01', 1, 1, 80, 4, 100);
insert into user_one_rep_max(id, valid_from, valid_to, user_id, exercise_type_id, weight, reps, one_rep_max) values (3, '2018-01-01', '9999-01-01', 1, 1, 80, 4, 100);
