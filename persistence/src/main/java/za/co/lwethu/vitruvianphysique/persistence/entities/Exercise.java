package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class Exercise extends BaseEntity {

    private Integer sequenceNumber;
    private WorkoutExercise workoutExercise;
    private ExerciseTemplate exerciseTemplate;
    private Integer restPeriod = 0;
    private ExerciseType type;
    private List<ExerciseSet> sets;

    @Column(nullable = false)
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public WorkoutExercise getWorkoutExercise() {
        return workoutExercise;
    }

    public void setWorkoutExercise(WorkoutExercise workoutExercise) {
        this.workoutExercise = workoutExercise;
    }

    @ManyToOne
    public ExerciseTemplate getExerciseTemplate() {
        return exerciseTemplate;
    }

    public void setExerciseTemplate(ExerciseTemplate exerciseTemplate) {
        this.exerciseTemplate = exerciseTemplate;
    }

    @Column(nullable = false)
    public Integer getRestPeriod() {
        return restPeriod;
    }

    public void setRestPeriod(Integer restPeriod) {
        this.restPeriod = restPeriod;
    }

    @ManyToOne
    public ExerciseType getType() {
        return type;
    }

    public void setType(ExerciseType type) {
        this.type = type;
    }

    @OneToMany(mappedBy = "exercise")
    public List<ExerciseSet> getSets() {
        return sets;
    }

    public void setSets(List<ExerciseSet> sets) {
        this.sets = sets;
    }
}
