package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseType;

public interface ExerciseTypeRepository extends JpaRepository<ExerciseType, Long>, JpaSpecificationExecutor<ExerciseType> {
}
