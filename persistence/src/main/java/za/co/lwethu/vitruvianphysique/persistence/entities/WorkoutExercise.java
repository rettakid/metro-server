package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class WorkoutExercise extends BaseEntity {

    private Workout workout;
    private Integer restPeriod = 0;
    private List<Exercise> exercises;

    @ManyToOne
    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

    @Column(nullable = false)
    public Integer getRestPeriod() {
        return restPeriod;
    }

    public void setRestPeriod(Integer restPeriod) {
        this.restPeriod = restPeriod;
    }

    @OneToMany(mappedBy = "workoutExercise")
    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

}
