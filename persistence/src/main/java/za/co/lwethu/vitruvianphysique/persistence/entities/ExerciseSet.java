package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.common.enums.DifficultyEnum;
import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;

@Entity
@Table
public class ExerciseSet extends BaseEntity {

    private Exercise exercise;
    private Integer sequenceNumber;
    private Integer restPeriod = 0;
    private Integer exercisePeriod = 0;
    private Double weight = 0.0;
    private Integer reps = 0;
    private Boolean toFailure = false;
    private DifficultyEnum difficulty;

    @ManyToOne
    @JoinColumn(nullable = false)
    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    @Column(nullable = false)
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @Column(nullable = false)
    public Integer getRestPeriod() {
        return restPeriod;
    }

    public void setRestPeriod(Integer restPeriod) {
        this.restPeriod = restPeriod;
    }

    public Integer getExercisePeriod() {
        return exercisePeriod;
    }

    public void setExercisePeriod(Integer exercisePeriod) {
        this.exercisePeriod = exercisePeriod;
    }

    @Column(nullable = false)
    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Column(nullable = false)
    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    @Column(nullable = false)
    public Boolean getToFailure() {
        return toFailure;
    }

    public void setToFailure(Boolean toFailure) {
        this.toFailure = toFailure;
    }

    @Enumerated(EnumType.ORDINAL)
    public DifficultyEnum getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(DifficultyEnum difficulty) {
        this.difficulty = difficulty;
    }

}
