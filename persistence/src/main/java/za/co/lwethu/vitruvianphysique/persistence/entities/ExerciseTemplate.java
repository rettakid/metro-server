package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class ExerciseTemplate extends BaseEntity {

    private Program program;
    private ExerciseType exerciseType;
    private MuscleGroup muscleGroup;
    private Boolean randomise;

    @ManyToOne
    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    @ManyToOne
    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

    @ManyToOne
    public MuscleGroup getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(MuscleGroup muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    public Boolean getRandomise() {
        return randomise;
    }

    public void setRandomise(Boolean randomise) {
        this.randomise = randomise;
    }
}
