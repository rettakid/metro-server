package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseSet;

public interface ExerciseSetRepository extends JpaRepository<ExerciseSet, Long>, JpaSpecificationExecutor<ExerciseSet> {
}
