package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;

@Entity
@Table
public class ExerciseMuscleGroupImpact extends BaseEntity {

    private ExerciseType exerciseType;
    private MuscleGroup muscleGroup;
    private Boolean symmetrical;
    private Integer impact;

    @ManyToOne
    @JoinColumn(nullable = false)
    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public MuscleGroup getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(MuscleGroup muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    @Column(nullable = false)
    public Boolean getSymmetrical() {
        return symmetrical;
    }

    public void setSymmetrical(Boolean symmetrical) {
        this.symmetrical = symmetrical;
    }

    @Column(nullable = false)
    public Integer getImpact() {
        return impact;
    }

    public void setImpact(Integer impact) {
        this.impact = impact;
    }

}
