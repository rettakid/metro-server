package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class ExerciseType extends BaseEntity {

    private String name;
    private String description;
    private Integer bodyWeighImpact;
    private Boolean requiresEquipment;
    private Boolean requiresWeights;
    private Boolean isometric;
    private Boolean timeBased;
    private Integer concentricPeriod;
    private Integer concentricHoldPeriod;
    private Integer eccentricPeriod;
    private Integer eccentricHoldPeriod;
    private ImageFile image;
    private VideoFile video;
    private List<ExerciseMuscleGroupImpact> muscleGroupImpacts;

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(nullable = false, length = 3)
    public Integer getBodyWeighImpact() {
        return bodyWeighImpact;
    }

    public void setBodyWeighImpact(Integer bodyWeighImpact) {
        this.bodyWeighImpact = bodyWeighImpact;
    }

    @Column(nullable = false)
    public Boolean getRequiresEquipment() {
        return requiresEquipment;
    }

    public void setRequiresEquipment(Boolean requiresEquipment) {
        this.requiresEquipment = requiresEquipment;
    }

    @Column(nullable = false)
    public Boolean getRequiresWeights() {
        return requiresWeights;
    }

    public void setRequiresWeights(Boolean requiresWeights) {
        this.requiresWeights = requiresWeights;
    }

    @Column(nullable = false)
    public Boolean getIsometric() {
        return isometric;
    }

    public void setIsometric(Boolean isometric) {
        this.isometric = isometric;
    }

    @Column(nullable = false)
    public Boolean getTimeBased() {
        return timeBased;
    }

    public void setTimeBased(Boolean timeBased) {
        this.timeBased = timeBased;
    }

    @Column(nullable = false)
    public Integer getConcentricPeriod() {
        return concentricPeriod;
    }

    public void setConcentricPeriod(Integer concentricPeriod) {
        this.concentricPeriod = concentricPeriod;
    }

    @Column(nullable = false)
    public Integer getConcentricHoldPeriod() {
        return concentricHoldPeriod;
    }

    public void setConcentricHoldPeriod(Integer concentricHoldPeriod) {
        this.concentricHoldPeriod = concentricHoldPeriod;
    }

    @Column(nullable = false)
    public Integer getEccentricPeriod() {
        return eccentricPeriod;
    }

    public void setEccentricPeriod(Integer eccentricPeriod) {
        this.eccentricPeriod = eccentricPeriod;
    }

    @Column(nullable = false)
    public Integer getEccentricHoldPeriod() {
        return eccentricHoldPeriod;
    }

    public void setEccentricHoldPeriod(Integer eccentricHoldPeriod) {
        this.eccentricHoldPeriod = eccentricHoldPeriod;
    }

    @ManyToOne
    public ImageFile getImage() {
        return image;
    }

    public void setImage(ImageFile image) {
        this.image = image;
    }

    @ManyToOne
    public VideoFile getVideo() {
        return video;
    }

    public void setVideo(VideoFile video) {
        this.video = video;
    }

    @OneToMany
    public List<ExerciseMuscleGroupImpact> getMuscleGroupImpacts() {
        return muscleGroupImpacts;
    }

    public void setMuscleGroupImpacts(List<ExerciseMuscleGroupImpact> muscleGroupImpacts) {
        this.muscleGroupImpacts = muscleGroupImpacts;
    }
}
