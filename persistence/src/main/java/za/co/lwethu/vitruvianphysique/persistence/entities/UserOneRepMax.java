package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;

@Entity
@Table
public class UserOneRepMax extends BaseEntity {

    private User user;
    private ExerciseType exerciseType;
    private Float weight;
    private Integer reps;
    private Float oneRepMax;

    @ManyToOne
    @JoinColumn(nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    @Column(nullable = false)
    public Float getOneRepMax() {
        return oneRepMax;
    }

    public void setOneRepMax(Float oneRepMax) {
        this.oneRepMax = oneRepMax;
    }
}
