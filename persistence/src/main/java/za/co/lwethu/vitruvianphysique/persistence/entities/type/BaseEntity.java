package za.co.lwethu.vitruvianphysique.persistence.entities.type;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class BaseEntity {

    private Long id;
    private Date validFrom;
    private Date validTo;

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "gen_sequence",initialValue = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    @Column(nullable = false)
    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

}
