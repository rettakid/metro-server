package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class WorkoutCycle extends BaseEntity {

    private Workout workout;
    private Cycle cycle;

    @ManyToOne
    @JoinColumn(nullable = false)
    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public Cycle getCycle() {
        return cycle;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }
}
