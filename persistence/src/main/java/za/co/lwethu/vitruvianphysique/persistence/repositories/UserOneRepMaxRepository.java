package za.co.lwethu.vitruvianphysique.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import za.co.lwethu.vitruvianphysique.persistence.entities.UserOneRepMax;

public interface UserOneRepMaxRepository extends JpaRepository<UserOneRepMax, Long>, JpaSpecificationExecutor<UserOneRepMax> {
}
