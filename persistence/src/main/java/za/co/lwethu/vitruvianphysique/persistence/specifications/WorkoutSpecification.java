package za.co.lwethu.vitruvianphysique.persistence.specifications;

import org.springframework.data.jpa.domain.Specification;
import za.co.lwethu.vitruvianphysique.persistence.entities.Workout;
import za.co.lwethu.vitruvianphysique.persistence.entities.Workout_;

import java.util.Date;

public class WorkoutSpecification {

    private WorkoutSpecification() {
    }

    public static Specification<Workout> workoutBetween(Date from, Date to) {
        return (Specification<Workout>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.between(root.get(Workout_.workoutDate), from, to);
    }

    public static Specification<Workout> workoutDateIsNull() {
        return (Specification<Workout>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNull(root.get(Workout_.workoutDate));
    }

}
