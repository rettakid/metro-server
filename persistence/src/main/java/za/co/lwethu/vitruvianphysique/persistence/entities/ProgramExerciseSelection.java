package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.common.enums.LiftTypeEnum;
import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;

@Entity
@Table
public class ProgramExerciseSelection extends BaseEntity {

    private UserProgram userProgram;
    private ExerciseType exerciseType;

    @ManyToOne
    @JoinColumn(nullable = false)
    public UserProgram getUserProgram() {
        return userProgram;
    }

    public void setUserProgram(UserProgram userProgram) {
        this.userProgram = userProgram;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

}
