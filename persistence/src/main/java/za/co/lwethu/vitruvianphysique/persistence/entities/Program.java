package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;
import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class Program extends BaseEntity {

    private String name;
    private Boolean premium;
    private User owner;
    private List<ProgramWorkoutSchedule> workoutSchedule;

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public Boolean getPremium() {
        return premium;
    }

    public void setPremium(Boolean premium) {
        this.premium = premium;
    }

    @ManyToOne
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @OneToMany(mappedBy = "program")
    public List<ProgramWorkoutSchedule> getWorkoutSchedule() {
        return workoutSchedule;
    }

    public void setWorkoutSchedule(List<ProgramWorkoutSchedule> workoutSchedule) {
        this.workoutSchedule = workoutSchedule;
    }

}
