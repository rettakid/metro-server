package za.co.lwethu.vitruvianphysique.persistence.specifications;

import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

public class BaseSpecification {

    public static <T> Specification<T> isActive() {
        Date now = new Date();
        return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                criteriaBuilder.lessThanOrEqualTo(root.get("validFrom"), now),
                criteriaBuilder.greaterThanOrEqualTo(root.get("validTo"), now)
        );
    }

}
