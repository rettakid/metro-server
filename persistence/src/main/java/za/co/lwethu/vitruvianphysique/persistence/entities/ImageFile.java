package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class ImageFile extends BaseEntity {

    private FileObject file;

    @ManyToOne
    @JoinColumn(nullable = false)
    public FileObject getFile() {
        return file;
    }

    public void setFile(FileObject file) {
        this.file = file;
    }

}
