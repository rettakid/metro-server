package za.co.lwethu.vitruvianphysique.persistence.entities;

import za.co.lwethu.vitruvianphysique.persistence.entities.type.BaseEntity;

import javax.persistence.*;

@Entity
@Table
public class VideoFile extends BaseEntity {

    private FileObject file;
    private FileObject thumbnail;

    @ManyToOne
    @JoinColumn(nullable = false)
    public FileObject getFile() {
        return file;
    }

    public void setFile(FileObject file) {
        this.file = file;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public FileObject getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(FileObject thumbnail) {
        this.thumbnail = thumbnail;
    }
}
