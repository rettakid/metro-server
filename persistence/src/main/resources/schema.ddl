create sequence gen_sequence start with 1 increment by 50

create table cycle (
id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
name varchar(255) not null,
sequence_number integer not null,
cycle_id bigint,
primary key (id)
);

create table cycle_cycles (
cycle_id bigint not null,
cycles_id bigint not null
);

create table exercise (
id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
rest_period integer,
sequence_number integer not null,
exercise_template_id bigint,
type_id bigint,
workout_exercise_id bigint not null,
primary key (id)
);

create table exercise_sets (
exercise_id bigint not null,
sets_id bigint not null
);

create table exercise_muscle_group_impact (
id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
impact integer not null,
symmetrical boolean not null,
exercise_type_id bigint not null,
muscle_group_id bigint not null,
primary key (id)
);

create table exercise_set (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
difficulty integer,
exercise_period integer,
reps integer not null,
rest_period integer not null,
sequence_number integer not null,
to_failure boolean not null,
weight integer not null,
exercise_id bigint not null,
primary key (id)
);

create table exercise_template (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
randomise boolean,
exercise_type_id bigint,
muscle_group_id bigint,
program_id bigint,
primary key (id)
);

create table exercise_type (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
body_weigh_impact integer not null,
description varchar(255) not null,
isometric boolean not null,
name varchar(255) not null,
requires_equipment boolean not null,
requires_weights boolean not null,
time_based boolean not null,
concentric_period integer not null,
concentric_hold_period integer not null,
eccentric_period integer not null,
eccentric_hold_period integer not null,
image_id bigint,
video_id bigint,
primary key (id)
);

create table exercise_type_muscle_group_impacts (exercise_type_id bigint not null,
muscle_group_impacts_id bigint not null
);

create table file_object (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
location varchar(255) not null,
mime_type varchar(255) not null,
name varchar(255) not null,
primary key (id)
);

create table image_file (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
file_id bigint not null,
primary key (id)
);

create table muscle_group (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
description varchar(255) not null,
name varchar(255) not null,
image_id bigint,
primary key (id)
);

create table program (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
name varchar(255) not null,
premium boolean not null,
owner_id bigint,
primary key (id)
);

create table program_exercise_selection (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
exercise_type_id bigint not null,
user_program_id bigint not null,
primary key (id)
);

create table program_workout_schedule (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
cycle_id bigint not null,
program_id bigint not null,
primary key (id)
);

create table user (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
date_of_birth timestamp not null,
email varchar(255) not null,
name varchar(255) not null,
password varchar(255) not null,
surname varchar(255) not null,
image_id bigint,
primary key (id)
);

create table user_body_fat (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
body_fat float not null,
user_id bigint not null,
primary key (id)
);

create table user_one_rep_max (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
one_rep_max float not null,
reps integer,
weight float,
exercise_type_id bigint not null,
user_id bigint not null,
primary key (id)
);

create table user_program (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
program_id bigint not null,
user_id bigint not null,
primary key (id)
);

create table user_weight (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
weight float not null,
user_id bigint not null,
primary key (id)
);

create table video_file (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
file_id bigint not null,
thumbnail_id bigint not null,
primary key (id)
);

create table workout (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
name varchar(255),
sequence_number integer,
workout_date timestamp,
user_id bigint,
primary key (id)
);

create table workout_workout_exercises (workout_id bigint not null,
workout_exercises_id bigint not null
);

create table workout_cycle (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
cycle_id bigint not null,
workout_id bigint not null,
primary key (id)
);

create table workout_exercise (id bigint not null,
valid_from timestamp not null,
valid_to timestamp not null,
rest_period integer not null,
workout_id bigint,
primary key (id)
);

create table workout_exercise_exercises (workout_exercise_id bigint not null,
exercises_id bigint not null
);


-- alter table cycle_cycles add constraint UK_kcg9wbv8dvld28be7fmom0ptv unique (cycles_id)
-- alter table exercise_sets add constraint UK_kg5p8rf465lykwmpcl9faxl0s unique (sets_id)
-- alter table exercise_type_muscle_group_impacts add constraint UK_ko3haleul84upqtiwfn5ysr8p unique (muscle_group_impacts_id)
-- alter table user add constraint UK_i5xeor1kbd7mof6edpvkp3wsd unique (image_id)
-- alter table workout_workout_exercises add constraint UK_7qq6dwaj7tq7uj3672essyux6 unique (workout_exercises_id)
-- alter table workout_exercise_exercises add constraint UK_netty2ybyvxnlt4jgbldr8poh unique (exercises_id)
-- alter table cycle add constraint FK76kfpjbeo9qc56c13s5yn7lkh foreign key (cycle_id) references cycle
-- alter table cycle_cycles add constraint FK768fjs1k0b9gh4xit45j1ov4d foreign key (cycles_id) references cycle
-- alter table cycle_cycles add constraint FKd828lmqqcil82xr1e7yfar08p foreign key (cycle_id) references cycle
-- alter table exercise add constraint FKq151ipvtwabyo2u8gppd6c2dn foreign key (exercise_template_id) references exercise_template
-- alter table exercise add constraint FK3p2so25iuwu4aa85tfe4mi9ua foreign key (type_id) references exercise_type
-- alter table exercise add constraint FKnkafyh6jsqkq14a2sf5g4m9rq foreign key (workout_exercise_id) references workout_exercise
-- alter table exercise_sets add constraint FKim10mfh9lep6j8vm1eq2ox86d foreign key (sets_id) references exercise_set
-- alter table exercise_sets add constraint FKen2r5m46sras31j23j0gxeeyr foreign key (exercise_id) references exercise
-- alter table exercise_muscle_group_impact add constraint FK5vo17djl26w16r4qquba4ehgi foreign key (exercise_type_id) references exercise_type
-- alter table exercise_muscle_group_impact add constraint FKrrq1oom0d7e3qel8ey8uh6sy0 foreign key (muscle_group_id) references muscle_group
-- alter table exercise_set add constraint FK2l9crqua2gdu9b38sfd4954s7 foreign key (exercise_id) references exercise
-- alter table exercise_template add constraint FKhscp5lmviier4reynfhg6dn0o foreign key (exercise_type_id) references exercise_type
-- alter table exercise_template add constraint FK7s1wno72libc8tw00ggxgkoak foreign key (muscle_group_id) references muscle_group
-- alter table exercise_template add constraint FK1g1yve9uitr09y3knc80bkupo foreign key (program_id) references program
-- alter table exercise_type add constraint FKlta64edx262pytfgfaav4orr2 foreign key (image_id) references image_file
-- alter table exercise_type add constraint FKpb0jo2crqehd3wpqt7mq84xwk foreign key (video_id) references video_file
-- alter table exercise_type_muscle_group_impacts add constraint FKe75ktaicb8twhjrqyb83d9b17 foreign key (muscle_group_impacts_id) references exercise_muscle_group_impact
-- alter table exercise_type_muscle_group_impacts add constraint FKa92og0dd1r114wgawspv5irdf foreign key (exercise_type_id) references exercise_type
-- alter table image_file add constraint FK2xmq04guh8xoetf99jju3qnfs foreign key (file_id) references file_object
-- alter table muscle_group add constraint FKyvmxx4h82j0ebi6mtv38kcax foreign key (image_id) references image_file
-- alter table program add constraint FKocss44jbj5jp4ho1maeyado19 foreign key (owner_id) references user
-- alter table program_exercise_selection add constraint FKrgaix821h80irie1xmtcjhx9t foreign key (exercise_type_id) references exercise_type
-- alter table program_exercise_selection add constraint FK5r25gtasyrbrytcb43cb7uts0 foreign key (user_program_id) references user_program
-- alter table program_workout_schedule add constraint FKkgr36vqlc20dld536cxngu8k foreign key (cycle_id) references cycle
-- alter table program_workout_schedule add constraint FKj9csros613nre8y8o153pept7 foreign key (program_id) references program
-- alter table user add constraint FKsv1iwrl3mbgpeakcht7ldnlmw foreign key (image_id) references file_object
-- alter table user_body_fat add constraint FKcmf18nkal7wr0yw9l56mt0u2k foreign key (user_id) references user
-- alter table user_one_rep_max add constraint FKgywfl94j73lp9stp0ai8a8bu5 foreign key (exercise_type_id) references exercise_type
-- alter table user_one_rep_max add constraint FK94v64eo5vnvunh8g1k738grud foreign key (user_id) references user
-- alter table user_program add constraint FK3w6262yvh37mmec2ukvryobdo foreign key (program_id) references program
-- alter table user_program add constraint FKj0gfrrt70n7nuonltud91h97w foreign key (user_id) references user
-- alter table user_weight add constraint FKiqbjpxwgcnvq974ebsha5c1by foreign key (user_id) references user
-- alter table video_file add constraint FKdg8k0sirwwn1mxsr059lmokok foreign key (file_id) references file_object
-- alter table video_file add constraint FKhp7uewc93yfcd88tenbbce221 foreign key (thumbnail_id) references file_object
-- alter table workout add constraint FKfd6lahc24vib7n7vw2ekecn00 foreign key (user_id) references user
-- alter table workout_workout_exercises add constraint FKl99drd0rphaqqf7or8k5q2cse foreign key (workout_exercises_id) references workout_exercise
-- alter table workout_workout_exercises add constraint FK7qp48qcgo3e8eqv2ij2q6mwfh foreign key (workout_id) references workout
-- alter table workout_cycle add constraint FKd5vxjx7eoxiluxxgev3yys8gg foreign key (cycle_id) references cycle
-- alter table workout_cycle add constraint FK3grw01rjupmmlyuviv90vhjv1 foreign key (workout_id) references workout
-- alter table workout_exercise add constraint FKqultuq4g6w47iqdaf0vb8ff3j foreign key (workout_id) references workout
-- alter table workout_exercise_exercises add constraint FKpr9rfkttsc2fkfcqg5iua432p foreign key (exercises_id) references exercise
-- alter table workout_exercise_exercises add constraint FKcs54n5s7sycldfe25rgnr290k foreign key (workout_exercise_id) references workout_exercise