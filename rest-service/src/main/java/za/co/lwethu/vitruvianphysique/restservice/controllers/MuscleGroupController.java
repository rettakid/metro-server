package za.co.lwethu.vitruvianphysique.restservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.lwethu.vitruvianphysique.common.dtos.MuscleGroupDto;
import za.co.lwethu.vitruvianphysique.service.MuscleGroupService;

import java.util.List;

@RestController
@RequestMapping("muscle-groups")
public class MuscleGroupController {

    private final static Logger logger = LoggerFactory.getLogger(MuscleGroupController.class);

    private final MuscleGroupService muscleGroupService;

    @Autowired
    public MuscleGroupController(MuscleGroupService muscleGroupService) {
        this.muscleGroupService = muscleGroupService;
    }

    @GetMapping
    public List<MuscleGroupDto> getMuscleGroups() {
        logger.info("getMuscleGroups");
        return muscleGroupService.getMuscleGroups();
    }

}
