package za.co.lwethu.vitruvianphysique.restservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseTypeDto;
import za.co.lwethu.vitruvianphysique.service.ExerciseTypeService;

import java.util.List;

@RestController
@RequestMapping("exercise-types")
public class ExerciseTypeController {

    private final static Logger logger = LoggerFactory.getLogger(ExerciseTypeController.class);

    private final ExerciseTypeService exerciseTypeService;

    @Autowired
    public ExerciseTypeController(ExerciseTypeService exerciseTypeService) {
        this.exerciseTypeService = exerciseTypeService;
    }

    @GetMapping
    public List<ExerciseTypeDto> getAllExerciseType() {
        logger.info("getAllExerciseType");
        return exerciseTypeService.getAllExerciseTypes();
    }

}
