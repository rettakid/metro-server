package za.co.lwethu.vitruvianphysique.restservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutDto;
import za.co.lwethu.vitruvianphysique.common.dtos.vos.WorkoutDayVo;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.service.WorkoutService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("workouts")
public class WorkoutController {

    private final static Logger logger = LoggerFactory.getLogger(WorkoutController.class);

    private final WorkoutService workoutService;

    @Autowired
    public WorkoutController(WorkoutService workoutService) {
        this.workoutService = workoutService;
    }

    @GetMapping("days")
    public List<WorkoutDayVo> getWorkoutDays(
            @RequestParam("from-date") @DateTimeFormat(pattern = DateUtilz.DATE_TIME_FORMAT) Date fromDate
            , @RequestParam("to-date") @DateTimeFormat(pattern = DateUtilz.DATE_TIME_FORMAT) Date toDate
            , @RequestParam(required = false) String[] params) {
        logger.info("getWorkoutDays - {},{}", fromDate, toDate);
        return workoutService.getWorkoutDays(fromDate, toDate);
    }

    @GetMapping("{id}")
    public WorkoutDto getWorkout(@PathVariable Long id) {
        logger.info("getWorkout");
        return workoutService.getWorkout(id);
    }

    @GetMapping
    public WorkoutDto getWorkout() {
        logger.info("getWorkout");
        WorkoutDto workout = new WorkoutDto();
        workout.setWorkoutDate(new Date());
        return workout;
    }

    @PostMapping
    public WorkoutDto saveWorkout(@RequestBody WorkoutDto workout) {
        logger.info("saveWorkout - {}", workout);
        return workoutService.saveWorkout(workout);
    }

}
