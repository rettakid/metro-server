package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseTypeDto;

import java.util.List;

public interface ExerciseTypeService {
    List<ExerciseTypeDto> getAllExerciseTypes();
}
