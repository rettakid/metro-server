package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.UserOneRepMaxDto;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.persistence.entities.UserOneRepMax;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseTypeRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserOneRepMaxRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserRepository;
import za.co.lwethu.vitruvianphysique.service.UserOneRepMaxService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserOneRepMaxServiceImpl implements UserOneRepMaxService {

    private final static Logger logger = LoggerFactory.getLogger(UserOneRepMaxServiceImpl.class);
    private AtlasMapper atlasMapper = new AtlasMapper();

    private final UserOneRepMaxRepository userOneRepMaxRepository;
    private final ExerciseTypeRepository exerciseTypeRepository;
    private final UserRepository userRepository;

    @Autowired
    public UserOneRepMaxServiceImpl(UserOneRepMaxRepository userOneRepMaxRepository, ExerciseTypeRepository exerciseTypeRepository, UserRepository userRepository) {
        this.userOneRepMaxRepository = userOneRepMaxRepository;
        this.exerciseTypeRepository = exerciseTypeRepository;
        this.userRepository = userRepository;
    }

    @Override
    public UserOneRepMaxDto createUserOneRepMax(UserOneRepMaxDto userOneRepMaxDto) {
        logger.debug("createUserOneRepMax - {}", userOneRepMaxDto);
        return atlasMapper.map(userOneRepMaxRepository.save(readyObjectCreation(userOneRepMaxDto)), UserOneRepMaxDto.class);
    }

    @Override
    public List<UserOneRepMaxDto> createUserOneRepMaxs(List<UserOneRepMaxDto> userOneRepMaxDtos) {
        logger.debug("createUserOneRepMaxs - {}", userOneRepMaxDtos);
        List<UserOneRepMax> userOneRepMaxes = userOneRepMaxDtos.stream().map(this::readyObjectCreation).collect(Collectors.toList());
        return atlasMapper.map(userOneRepMaxRepository.saveAll(userOneRepMaxes), UserOneRepMaxDto.class);
    }

    private UserOneRepMax readyObjectCreation(UserOneRepMaxDto userOneRepMaxDto) {
        UserOneRepMax userOneRepMax = new UserOneRepMax();
        userOneRepMax.setExerciseType(exerciseTypeRepository.getOne(userOneRepMaxDto.getExerciseType().getId()));
        userOneRepMax.setUser(userRepository.getOne(userOneRepMaxDto.getUser().getId()));
        userOneRepMax.setReps(userOneRepMaxDto.getReps());
        userOneRepMax.setWeight(userOneRepMaxDto.getWeight());
        userOneRepMax.setOneRepMax(userOneRepMaxDto.getOneRepMax());
        userOneRepMax.setValidFrom(new Date());
        userOneRepMax.setValidTo(DateUtilz.END_OF_TIME);
        return userOneRepMax;
    }

}
