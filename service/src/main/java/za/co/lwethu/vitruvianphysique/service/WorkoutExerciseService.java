package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutExerciseDto;

public interface WorkoutExerciseService {
    WorkoutExerciseDto saveWorkoutExercise(WorkoutExerciseDto workoutExerciseDto);
}
