package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.UserProgramDto;

public interface UserProgramService {
    UserProgramDto createUserProgram(UserProgramDto userProgramDto);
}
