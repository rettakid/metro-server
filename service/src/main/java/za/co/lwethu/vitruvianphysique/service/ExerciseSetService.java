package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseSetDto;

public interface ExerciseSetService {
    ExerciseSetDto saveExerciseSet(ExerciseSetDto exerciseSetDto);
}
