package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.MuscleGroupDto;
import za.co.lwethu.vitruvianphysique.persistence.repositories.MuscleGroupRepository;
import za.co.lwethu.vitruvianphysique.service.MuscleGroupService;

import java.util.List;

import static za.co.lwethu.vitruvianphysique.persistence.specifications.BaseSpecification.isActive;

@Service
@Transactional
public class MuscleGroupServiceImpl implements MuscleGroupService {

    private Logger logger = LoggerFactory.getLogger(MuscleGroupServiceImpl.class);
    private final static AtlasMapper atlasMapper = new AtlasMapper();

    private final MuscleGroupRepository muscleGroupRepository;

    @Autowired
    public MuscleGroupServiceImpl(MuscleGroupRepository muscleGroupRepository) {
        this.muscleGroupRepository = muscleGroupRepository;
    }

    @Override
    public List<MuscleGroupDto> getMuscleGroups() {
        logger.debug("getMuscleGroups");
        return atlasMapper.map(muscleGroupRepository.findAll(isActive()), MuscleGroupDto.class);
    }

}
