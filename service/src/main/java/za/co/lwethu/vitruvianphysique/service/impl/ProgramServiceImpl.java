package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ProgramRepository;
import za.co.lwethu.vitruvianphysique.service.ProgramService;

@Service
@Transactional
public class ProgramServiceImpl implements ProgramService {

    private final static Logger logger = LoggerFactory.getLogger(ProgramServiceImpl.class);

    private ProgramRepository programRepository;

}
