package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.MuscleGroupDto;

import java.util.List;

public interface MuscleGroupService {
    List<MuscleGroupDto> getMuscleGroups();
}
