package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.UserProgramDto;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.persistence.entities.UserProgram;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ProgramRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserProgramRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserRepository;
import za.co.lwethu.vitruvianphysique.service.UserProgramService;

import java.util.Date;

@Service
@Transactional
public class UserProgramServiceImpl implements UserProgramService {

    private static final Logger logger = LoggerFactory.getLogger(UserProgramServiceImpl.class);
    private final AtlasMapper atlasMapper = new AtlasMapper();

    private final UserProgramRepository userProgramRepository;
    private final ProgramRepository programRepository;
    private final UserRepository userRepository;

    @Autowired
    public UserProgramServiceImpl(UserProgramRepository userProgramRepository, ProgramRepository programRepository, UserRepository userRepository) {
        this.userProgramRepository = userProgramRepository;
        this.programRepository = programRepository;
        this.userRepository = userRepository;
    }

    @Override
    public UserProgramDto createUserProgram(UserProgramDto input) {
        logger.debug("createUserProgram - {}", input);
        UserProgram userProgram = new UserProgram();
        userProgram.setProgram(programRepository.getOne(input.getProgram().getId()));
        userProgram.setUser(userRepository.getOne(input.getUser().getId()));
        userProgram.setValidFrom(new Date());
        userProgram.setValidTo(DateUtilz.END_OF_TIME);
        return atlasMapper.map(userProgramRepository.save(userProgram), UserProgramDto.class);
    }
}
