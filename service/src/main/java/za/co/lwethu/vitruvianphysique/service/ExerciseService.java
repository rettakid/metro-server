package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseDto;

public interface ExerciseService {
    ExerciseDto saveExercise(ExerciseDto exerciseDto);
}
