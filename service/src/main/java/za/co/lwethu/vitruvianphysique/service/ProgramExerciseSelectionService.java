package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.ProgramExerciseSelectionDto;

public interface ProgramExerciseSelectionService {
    ProgramExerciseSelectionDto createProgramExerciseSelection(ProgramExerciseSelectionDto programExerciseSelectionDto);
}
