package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.ProgramExerciseSelectionDto;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.persistence.entities.ProgramExerciseSelection;
import za.co.lwethu.vitruvianphysique.persistence.repositories.*;
import za.co.lwethu.vitruvianphysique.service.ProgramExerciseSelectionService;

import java.util.Date;

@Service
@Transactional
public class ProgramExerciseSelectionServiceImpl implements ProgramExerciseSelectionService {

    private static final Logger logger = LoggerFactory.getLogger(ProgramExerciseSelectionServiceImpl.class);
    private final AtlasMapper atlasMapper = new AtlasMapper();

    private final ProgramExerciseSelectionRepository programExerciseSelectionRepository;
    private final ExerciseTypeRepository exerciseTypeRepository;
    private final UserProgramRepository userProgramRepository;

    @Autowired
    public ProgramExerciseSelectionServiceImpl(ProgramExerciseSelectionRepository programExerciseSelectionRepository, ExerciseTypeRepository exerciseTypeRepository, UserProgramRepository userProgramRepository) {
        this.programExerciseSelectionRepository = programExerciseSelectionRepository;
        this.exerciseTypeRepository = exerciseTypeRepository;
        this.userProgramRepository = userProgramRepository;
    }

    @Override
    public ProgramExerciseSelectionDto createProgramExerciseSelection(ProgramExerciseSelectionDto programExerciseSelectionDto) {
        logger.debug("createProgramExerciseSelection - {}", programExerciseSelectionDto);
        ProgramExerciseSelection programExerciseSelection = new ProgramExerciseSelection();
        programExerciseSelection.setExerciseType(exerciseTypeRepository.getOne(programExerciseSelectionDto.getExerciseType().getId()));
        programExerciseSelection.setUserProgram(userProgramRepository.getOne(programExerciseSelectionDto.getUserProgram().getId()));
        programExerciseSelection.setValidFrom(new Date());
        programExerciseSelection.setValidTo(DateUtilz.END_OF_TIME);
        return atlasMapper.map(programExerciseSelectionRepository.save(programExerciseSelection), ProgramExerciseSelectionDto.class);
    }

}
