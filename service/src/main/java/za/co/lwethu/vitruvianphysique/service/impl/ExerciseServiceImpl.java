package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseDto;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.persistence.entities.Exercise;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseService;
import za.co.lwethu.vitruvianphysique.service.ExerciseSetService;

import java.util.Date;
import java.util.stream.Collectors;

@Service
@Transactional
public class ExerciseServiceImpl implements ExerciseService {

    private final static Logger logger = LoggerFactory.getLogger(ExerciseServiceImpl.class);
    private final AtlasMapper atlasMapper = new AtlasMapper();

    private final ExerciseRepository exerciseRepository;
    private final ExerciseSetService exerciseSetService;

    @Autowired
    public ExerciseServiceImpl(ExerciseRepository exerciseRepository, ExerciseSetService exerciseSetService) {
        this.exerciseRepository = exerciseRepository;
        this.exerciseSetService = exerciseSetService;
    }

    @Override
    public ExerciseDto saveExercise(ExerciseDto exerciseDto) {
        logger.debug("saveExercise - {}", exerciseDto);
        Exercise exercise = atlasMapper.map(exerciseDto, Exercise.class);
        if (exerciseDto.getId() == null) {
            exercise.setValidFrom(new Date());
            exercise.setValidTo(DateUtilz.END_OF_TIME);
        }
        exercise.setSets(null);
        exercise = exerciseRepository.save(exercise);
        ExerciseDto output = atlasMapper.map(exercise, ExerciseDto.class);
        output.setSets(
                exerciseDto.getSets().stream()
                        .peek(set -> set.setExercise(output))
                        .map(exerciseSetService::saveExerciseSet)
                        .collect(Collectors.toList())
        );
        return output;
    }

}
