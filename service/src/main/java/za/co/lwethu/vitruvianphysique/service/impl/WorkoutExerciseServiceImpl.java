package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutExerciseDto;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseSet;
import za.co.lwethu.vitruvianphysique.persistence.entities.WorkoutExercise;
import za.co.lwethu.vitruvianphysique.persistence.repositories.WorkoutExerciseRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseService;
import za.co.lwethu.vitruvianphysique.service.WorkoutExerciseService;

import java.util.Date;
import java.util.stream.Collectors;

@Service
@Transactional
public class WorkoutExerciseServiceImpl implements WorkoutExerciseService {

    private final static Logger logger = LoggerFactory.getLogger(WorkoutExerciseServiceImpl.class);
    private final AtlasMapper atlasMapper = new AtlasMapper();

    private final WorkoutExerciseRepository workoutExerciseRepository;
    private final ExerciseService exerciseService;

    @Autowired
    public WorkoutExerciseServiceImpl(WorkoutExerciseRepository workoutExerciseRepository, ExerciseService exerciseService) {
        this.workoutExerciseRepository = workoutExerciseRepository;
        this.exerciseService = exerciseService;
    }

    @Override
    public WorkoutExerciseDto saveWorkoutExercise(WorkoutExerciseDto workoutExerciseDto) {
        logger.debug("saveWorkoutExercise - {}", workoutExerciseDto);
        WorkoutExercise workoutExercise = atlasMapper.map(workoutExerciseDto, WorkoutExercise.class);
        if (workoutExerciseDto.getId() == null) {
            workoutExercise.setValidFrom(new Date());
            workoutExercise.setValidTo(DateUtilz.END_OF_TIME);
        }
        workoutExercise.setExercises(null);
        workoutExercise = workoutExerciseRepository.save(workoutExercise);

        WorkoutExerciseDto output = atlasMapper.map(workoutExercise, WorkoutExerciseDto.class);
        output.setExercises(
                workoutExerciseDto.getExercises().stream()
                        .peek(exercise -> exercise.setWorkoutExercise(output))
                        .map(exerciseService::saveExercise)
                        .collect(Collectors.toList())
        );
        return output;
    }

}
