package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.UserOneRepMaxDto;

import java.util.List;

public interface UserOneRepMaxService {
    UserOneRepMaxDto createUserOneRepMax(UserOneRepMaxDto userOneRepMax);

    List<UserOneRepMaxDto> createUserOneRepMaxs(List<UserOneRepMaxDto> userOneRepMaxDtos);
}
