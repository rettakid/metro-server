package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseSetDto;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseSet;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseSetRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseSetService;

import java.util.Date;

@Service
@Transactional
public class ExerciseSetServiceImpl implements ExerciseSetService {

    private final static Logger logger = LoggerFactory.getLogger(ExerciseSetServiceImpl.class);
    private final AtlasMapper atlasMapper = new AtlasMapper();

    private final ExerciseSetRepository exerciseSetRepository;

    @Autowired
    public ExerciseSetServiceImpl(ExerciseSetRepository exerciseSetRepository) {
        this.exerciseSetRepository = exerciseSetRepository;
    }

    @Override
    public ExerciseSetDto saveExerciseSet(ExerciseSetDto exerciseSetDto) {
        logger.debug("saveExerciseSet - {}", exerciseSetDto);
        ExerciseSet exerciseSet = atlasMapper.map(exerciseSetDto, ExerciseSet.class);
        if (exerciseSet.getId() == null) {
            exerciseSet.setValidFrom(new Date());
            exerciseSet.setValidTo(DateUtilz.END_OF_TIME);
        }
        return atlasMapper.map(exerciseSetRepository.save(exerciseSet), ExerciseSetDto.class);
    }
}
