package za.co.lwethu.vitruvianphysique.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseTypeDto;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseTypeRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseTypeService;

import java.util.List;

import static za.co.lwethu.vitruvianphysique.persistence.specifications.BaseSpecification.isActive;

@Service
@Transactional
public class ExerciseTypeServiceImpl implements ExerciseTypeService {

    private final static Logger logger = LoggerFactory.getLogger(ExerciseTypeServiceImpl.class);
    private final AtlasMapper atlasMapper = new AtlasMapper();

    private final ExerciseTypeRepository exerciseTypeRepository;

    @Autowired
    public ExerciseTypeServiceImpl(ExerciseTypeRepository exerciseTypeRepository) {
        this.exerciseTypeRepository = exerciseTypeRepository;
    }

    @Override
    public List<ExerciseTypeDto> getAllExerciseTypes() {
        logger.debug("getAllExerciseTypes");
        return atlasMapper.map(exerciseTypeRepository.findAll(isActive()), ExerciseTypeDto.class);
    }
}
