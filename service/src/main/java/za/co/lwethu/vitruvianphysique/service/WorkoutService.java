package za.co.lwethu.vitruvianphysique.service;

import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutDto;
import za.co.lwethu.vitruvianphysique.common.dtos.vos.WorkoutDayVo;

import java.util.Date;
import java.util.List;

public interface WorkoutService {
    List<WorkoutDayVo> getWorkoutDays(Date fromDate, Date toDate);

    WorkoutDto saveWorkout(WorkoutDto workoutDto);

    WorkoutDto getWorkout(Long workoutId);
}
