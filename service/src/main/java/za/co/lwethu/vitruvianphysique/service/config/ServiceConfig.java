package za.co.lwethu.vitruvianphysique.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import za.co.lwethu.vitruvianphysique.persistence.PersistenceApplication;

@Configuration
@ComponentScan("za.co.lwethu.vitruvianphysique.service")
@Import(PersistenceApplication.class)
public class ServiceConfig {
}
