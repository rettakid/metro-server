package za.co.lwethu.vitruvianphysique.service.impl;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.discovery.cas.common.atlasmapper.AtlasMapper;
import za.co.lwethu.vitruvianphysique.common.dtos.ProgramDto;
import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutDto;
import za.co.lwethu.vitruvianphysique.common.dtos.vos.WorkoutDayVo;
import za.co.lwethu.vitruvianphysique.common.utilz.DateUtilz;
import za.co.lwethu.vitruvianphysique.persistence.entities.Workout;
import za.co.lwethu.vitruvianphysique.persistence.entities.Workout_;
import za.co.lwethu.vitruvianphysique.persistence.repositories.WorkoutRepository;
import za.co.lwethu.vitruvianphysique.service.WorkoutExerciseService;
import za.co.lwethu.vitruvianphysique.service.WorkoutService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static za.co.lwethu.vitruvianphysique.persistence.specifications.WorkoutSpecification.workoutBetween;
import static za.co.lwethu.vitruvianphysique.persistence.specifications.WorkoutSpecification.workoutDateIsNull;

@Service
@Transactional
public class WorkoutServiceImpl implements WorkoutService {

    private static final Logger logger = LoggerFactory.getLogger(WorkoutServiceImpl.class);
    private final AtlasMapper atlasMapper = new AtlasMapper();

    private final WorkoutRepository workoutRepository;
    private final WorkoutExerciseService workoutExerciseService;

    @Autowired
    public WorkoutServiceImpl(WorkoutRepository workoutRepository, WorkoutExerciseService workoutExerciseService) {
        this.workoutRepository = workoutRepository;
        this.workoutExerciseService = workoutExerciseService;
    }

    @Override
    public List<WorkoutDayVo> getWorkoutDays(Date fromDateParam, Date toDateParam) {
        logger.debug("getWorkoutDays - {},{}", fromDateParam, toDateParam);
        DateTime fromDate = new DateTime(fromDateParam.getTime())
                .withHourOfDay(0)
                .withMinuteOfHour(0);
        DateTime toDate = new DateTime(toDateParam.getTime())
                .withHourOfDay(0)
                .withMinuteOfHour(0);
        DateTime todaysDate = new DateTime(new Date().getTime())
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        List<WorkoutDto> completedWorkouts = atlasMapper.map(workoutRepository.findAll(workoutBetween(fromDate.toDate(), toDate.toDate())), WorkoutDto.class);
        List<WorkoutDto> futureWorkouts = getFutureWorkouts();
        return IntStream.iterate(0, i -> i + 1)
                .limit(Days.daysBetween(fromDate, toDate).getDays() + 1)
                .mapToObj(i -> new WorkoutDayVo(fromDate.plusDays(i).toDate()
                        , getCompletedWorkoutsForDay(fromDate.plusDays(i).toDate(), completedWorkouts)
                        , getFutureWorkoutsForDay(Days.daysBetween(todaysDate, fromDate).getDays() + i, futureWorkouts)))
                .collect(Collectors.toList());
    }

    @Override
    public WorkoutDto saveWorkout(WorkoutDto workoutDto) {
        logger.debug("saveWorkout - {}", workoutDto);
        Workout workout = atlasMapper.map(workoutDto, Workout.class);
        if (workoutDto.getId() == null) {
            workout.setValidFrom(new Date());
            workout.setValidTo(DateUtilz.END_OF_TIME);
        }
        workout.setWorkoutExercises(null);
        workout = workoutRepository.save(workout);

        WorkoutDto output = atlasMapper.map(workout, WorkoutDto.class);
        output.setWorkoutExercises(
                workoutDto.getWorkoutExercises().stream()
                        .peek(workoutExercise -> workoutExercise.setWorkout(output))
                        .map(workoutExerciseService::saveWorkoutExercise)
                        .collect(Collectors.toList())
        );
        return output;
    }

    @Override
    public WorkoutDto getWorkout(Long workoutId) {
        logger.debug("getWorkout - {}", workoutId);
        return atlasMapper.map(workoutRepository.getOne(workoutId),WorkoutDto.class);
    }

    private List<WorkoutDto> getFutureWorkouts() {
        List<WorkoutDto> futureWorkouts = atlasMapper.map(workoutRepository.findAll(workoutDateIsNull(), Sort.by(Workout_.sequenceNumber.getName())), WorkoutDto.class);
        Map<ProgramDto, Integer> programStartingPoints = getProgramInitSequence(futureWorkouts);
        futureWorkouts = futureWorkouts.stream()
                .peek(workout -> workout.setSequenceNumber(workout.getSequenceNumber() - programStartingPoints.get(workout.getProgram())))
                .collect(Collectors.toList());
        return futureWorkouts;
    }

    private List<WorkoutDto> getCompletedWorkoutsForDay(Date date, List<WorkoutDto> completedWorkouts) {
        DateTime fromDate = new DateTime(date.getTime()).withHourOfDay(0).withMinuteOfHour(0);
        DateTime toDate = new DateTime(date.getTime()).withHourOfDay(23).withMinuteOfHour(59);
        return completedWorkouts.stream()
                .filter(workout -> fromDate.isBefore(workout.getWorkoutDate().getTime()) && toDate.isAfter(workout.getWorkoutDate().getTime()))
                .collect(Collectors.toList());
    }

    private List<WorkoutDto> getFutureWorkoutsForDay(Integer index, List<WorkoutDto> futureWorkouts) {
        return futureWorkouts.stream()
                .filter(workout -> workout.getSequenceNumber().equals(index))
                .collect(Collectors.toList());
    }

    private Map<ProgramDto, Integer> getProgramInitSequence(List<WorkoutDto> workouts) {
        Map<ProgramDto, Integer> programStartingPoints = new HashMap<>();
        Set<ProgramDto> programs = workouts.stream()
                .map(WorkoutDto::getProgram)
                .collect(Collectors.toSet());
        programs.forEach(program -> programStartingPoints.put(program, workouts.stream()
                .filter(workout -> workout.getProgram() == program)
                .mapToInt(WorkoutDto::getSequenceNumber)
                .min()
                .orElse(0)
        ));
        return programStartingPoints;
    }

}
