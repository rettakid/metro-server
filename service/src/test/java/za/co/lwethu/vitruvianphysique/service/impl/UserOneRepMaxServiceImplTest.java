package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseTypeDto;
import za.co.lwethu.vitruvianphysique.common.dtos.UserDto;
import za.co.lwethu.vitruvianphysique.common.dtos.UserOneRepMaxDto;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseType;
import za.co.lwethu.vitruvianphysique.persistence.entities.User;
import za.co.lwethu.vitruvianphysique.persistence.entities.UserOneRepMax;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseTypeRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserOneRepMaxRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserRepository;
import za.co.lwethu.vitruvianphysique.service.UserOneRepMaxService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserOneRepMaxServiceImplTest {

    private UserOneRepMaxService userOneRepMaxService;

    @Before
    public void setUp() throws Exception {
        UserOneRepMaxRepository userOneRepMaxRepository = mock(UserOneRepMaxRepository.class);
        ExerciseTypeRepository exerciseTypeRepository = mock(ExerciseTypeRepository.class);
        UserRepository userRepository = mock(UserRepository.class);
        userOneRepMaxService = new UserOneRepMaxServiceImpl(userOneRepMaxRepository, exerciseTypeRepository, userRepository);

        when(userRepository.getOne(any())).thenReturn(new User());
        when(exerciseTypeRepository.getOne(any())).thenReturn(new ExerciseType());
        when(userOneRepMaxRepository.save(any())).thenAnswer((Answer<UserOneRepMax>) invocationOnMock -> (UserOneRepMax) invocationOnMock.getArgument(0));
        when(userOneRepMaxRepository.saveAll(any())).thenAnswer((Answer<List<UserOneRepMax>>) invocationOnMock -> invocationOnMock.getArgument(0));
    }

    @Test
    public void createUserOneRepMax() throws Exception {
        UserOneRepMaxDto userOneRepMaxDto = new UserOneRepMaxDto();
        userOneRepMaxDto.setExerciseType(new ExerciseTypeDto());
        userOneRepMaxDto.setUser(new UserDto());
        userOneRepMaxDto.setReps(5);
        UserOneRepMaxDto userOneRepMax = userOneRepMaxService.createUserOneRepMax(userOneRepMaxDto);
        assertEquals(5, (long) userOneRepMax.getReps());
    }

    @Test
    public void createUserOneRepMaxs() throws Exception {
        UserOneRepMaxDto userOneRepMaxDto = new UserOneRepMaxDto();
        userOneRepMaxDto.setExerciseType(new ExerciseTypeDto());
        userOneRepMaxDto.setUser(new UserDto());
        userOneRepMaxDto.setReps(5);
        List<UserOneRepMaxDto> userOneRepMaxs = userOneRepMaxService.createUserOneRepMaxs(Collections.singletonList(userOneRepMaxDto));
        assertEquals(1, userOneRepMaxs.size());
    }

}