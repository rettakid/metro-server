package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import za.co.lwethu.vitruvianphysique.common.dtos.ProgramDto;
import za.co.lwethu.vitruvianphysique.common.dtos.UserDto;
import za.co.lwethu.vitruvianphysique.common.dtos.UserProgramDto;
import za.co.lwethu.vitruvianphysique.persistence.entities.Program;
import za.co.lwethu.vitruvianphysique.persistence.entities.User;
import za.co.lwethu.vitruvianphysique.persistence.entities.UserProgram;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ProgramRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserProgramRepository;
import za.co.lwethu.vitruvianphysique.persistence.repositories.UserRepository;
import za.co.lwethu.vitruvianphysique.service.UserProgramService;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserProgramServiceImplTest {

    private UserProgramService userProgramService;

    @Before
    public void setUp() throws Exception {
        UserProgramRepository userProgramRepository = mock(UserProgramRepository.class);
        UserRepository userRepository = mock(UserRepository.class);
        ProgramRepository programRepository = mock(ProgramRepository.class);
        userProgramService = new UserProgramServiceImpl(userProgramRepository, programRepository, userRepository);

        when(programRepository.getOne(any())).thenReturn(new Program());
        when(userRepository.getOne(any())).thenReturn(new User());
        when(userProgramRepository.save(any())).thenAnswer((Answer<UserProgram>) invocationOnMock -> invocationOnMock.getArgument(0));
    }

    @Test
    public void createUserProgram() {
        UserProgramDto input = new UserProgramDto();
        input.setProgram( new ProgramDto());
        input.setUser(new UserDto());
        UserProgramDto userProgramDto = userProgramService.createUserProgram(input);
        assertNotNull(userProgramDto);
    }

}