package za.co.lwethu.vitruvianphysique.service.impl;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;
import org.springframework.data.jpa.domain.Specification;
import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutDto;
import za.co.lwethu.vitruvianphysique.common.dtos.vos.WorkoutDayVo;
import za.co.lwethu.vitruvianphysique.persistence.entities.Workout;
import za.co.lwethu.vitruvianphysique.persistence.repositories.WorkoutRepository;
import za.co.lwethu.vitruvianphysique.service.WorkoutExerciseService;
import za.co.lwethu.vitruvianphysique.service.WorkoutService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WorkoutServiceImplTest {

    private WorkoutService workoutService;

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        WorkoutRepository workoutRepository = mock(WorkoutRepository.class);
        WorkoutExerciseService workoutExerciseService = mock(WorkoutExerciseService.class);
        workoutService = new WorkoutServiceImpl(workoutRepository, workoutExerciseService);

        DateTime dateTime = new DateTime();

        Workout workout1 = new Workout();
        workout1.setWorkoutDate(dateTime.plusDays(1).toDate());
        workout1.setName("Pull Day");

        Workout workout2 = new Workout();
        workout2.setWorkoutDate(dateTime.plusDays(1).toDate());

        Workout workout3 = new Workout();
        workout3.setWorkoutDate(dateTime.plusDays(3).toDate());

        when(workoutRepository.getOne(any(Long.class))).thenReturn(workout1);
        when(workoutRepository.findAll(any(Specification.class))).thenReturn(Arrays.asList(workout1, workout2, workout3));
        when(workoutRepository.save(any())).thenAnswer((Answer<Workout>) invocationOnMock -> (Workout) invocationOnMock.getArgument(0));
    }

    @Test
    public void getWorkoutsByDate() {
        DateTime dateTime = new DateTime();
        List<WorkoutDayVo> workouts = workoutService.getWorkoutDays(dateTime.toDate(), dateTime.plusDays(5).toDate());
        assertEquals(6, workouts.size());
    }

    @Test
    public void getWorkout() {
        WorkoutDto workout = workoutService.getWorkout(1L);
        assertEquals("Pull Day", workout.getName());
    }

    @Test
    public void saveWorkout() {
        WorkoutDto input = new WorkoutDto();
        input.setName("Pull Day");
        input.setWorkoutExercises(new ArrayList<>());
        WorkoutDto workoutDto = workoutService.saveWorkout(input);
        assertEquals("Pull Day", workoutDto.getName());
    }
}