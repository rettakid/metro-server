package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import za.co.lwethu.vitruvianphysique.common.dtos.WorkoutExerciseDto;
import za.co.lwethu.vitruvianphysique.persistence.entities.WorkoutExercise;
import za.co.lwethu.vitruvianphysique.persistence.repositories.WorkoutExerciseRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseService;
import za.co.lwethu.vitruvianphysique.service.WorkoutExerciseService;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WorkoutExerciseServiceImplTest {

    private WorkoutExerciseService workoutExerciseService;

    @Before
    public void setUp() throws Exception {
        WorkoutExerciseRepository workoutExerciseRepository = mock(WorkoutExerciseRepository.class);
        ExerciseService exerciseService = mock(ExerciseService.class);
        workoutExerciseService = new WorkoutExerciseServiceImpl(workoutExerciseRepository, exerciseService);

        when(workoutExerciseRepository.save(any())).thenAnswer((Answer<WorkoutExercise>) invocationOnMock -> (WorkoutExercise) invocationOnMock.getArgument(0));
    }

    @Test
    public void saveWorkoutExercise() {
        WorkoutExerciseDto input = new WorkoutExerciseDto();
        input.setRestPeriod(60);
        input.setExercises(new ArrayList<>());
        WorkoutExerciseDto workoutExerciseDto = workoutExerciseService.saveWorkoutExercise(input);
        assertEquals(60, (int) workoutExerciseDto.getRestPeriod());
    }
}