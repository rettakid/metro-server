package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.springframework.data.jpa.domain.Specification;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseTypeDto;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseType;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseTypeRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseTypeService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExerciseTypeServiceImplTest {

    private ExerciseTypeService exerciseTypeService;

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        ExerciseTypeRepository exerciseTypeRepository = mock(ExerciseTypeRepository.class);
        exerciseTypeService = new ExerciseTypeServiceImpl(exerciseTypeRepository);

        ExerciseType exerciseType = new ExerciseType();
        exerciseType.setName("Push Ups");

        when(exerciseTypeRepository.findAll(any(Specification.class))).thenReturn(Collections.singletonList(exerciseType));
    }

    @Test
    public void getAllExerciseTypes() {
        List<ExerciseTypeDto> exerciseTypes = exerciseTypeService.getAllExerciseTypes();
        assertEquals("Push Ups", exerciseTypes.get(0).getName());
    }
}