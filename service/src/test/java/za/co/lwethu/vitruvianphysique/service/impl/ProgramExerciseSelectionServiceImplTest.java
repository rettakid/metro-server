package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import za.co.lwethu.vitruvianphysique.common.dtos.*;
import za.co.lwethu.vitruvianphysique.common.enums.LiftTypeEnum;
import za.co.lwethu.vitruvianphysique.persistence.entities.*;
import za.co.lwethu.vitruvianphysique.persistence.repositories.*;
import za.co.lwethu.vitruvianphysique.service.ProgramExerciseSelectionService;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProgramExerciseSelectionServiceImplTest {

    private ProgramExerciseSelectionService programExerciseSelectionService;

    @Before
    public void setUp() throws Exception {
        ProgramExerciseSelectionRepository programExerciseSelectionRepository = mock(ProgramExerciseSelectionRepository.class);
        ExerciseTypeRepository exerciseTypeRepository = mock(ExerciseTypeRepository.class);
        UserProgramRepository userProgramRepository = mock(UserProgramRepository.class);
        programExerciseSelectionService = new ProgramExerciseSelectionServiceImpl(programExerciseSelectionRepository, exerciseTypeRepository, userProgramRepository);

        when(exerciseTypeRepository.getOne(any())).thenReturn(new ExerciseType());
        when(userProgramRepository.getOne(any())).thenReturn(new UserProgram());
        when(programExerciseSelectionRepository.save(any())).thenAnswer((Answer<ProgramExerciseSelection>) invocationOnMock -> invocationOnMock.getArgument(0));
    }

    @Test
    public void createProgramExerciseSelection() throws Exception {
        ProgramExerciseSelectionDto input = new ProgramExerciseSelectionDto();
        input.setExerciseType(new ExerciseTypeDto());
        input.setUserProgram(new UserProgramDto());
        ProgramExerciseSelectionDto programExerciseSelection = programExerciseSelectionService.createProgramExerciseSelection(input);
        assertNotNull(programExerciseSelection);
    }
}