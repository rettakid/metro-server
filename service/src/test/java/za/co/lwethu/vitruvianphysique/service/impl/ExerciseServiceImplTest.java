package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseDto;
import za.co.lwethu.vitruvianphysique.persistence.entities.Exercise;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseService;
import za.co.lwethu.vitruvianphysique.service.ExerciseSetService;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExerciseServiceImplTest {

    private ExerciseService exerciseService;

    @Before
    public void setUp() throws Exception {
        ExerciseRepository exerciseRepository = mock(ExerciseRepository.class);
        ExerciseSetService exerciseSetService = mock(ExerciseSetService.class);
        exerciseService = new ExerciseServiceImpl(exerciseRepository, exerciseSetService);

        when(exerciseRepository.save(any())).thenAnswer((Answer<Exercise>) invocationOnMock -> (Exercise) invocationOnMock.getArgument(0));
    }

    @Test
    public void saveExercise() {
        ExerciseDto input = new ExerciseDto();
        input.setRestPeriod(60);
        input.setSets(new ArrayList<>());
        ExerciseDto exerciseDto = exerciseService.saveExercise(input);
        assertEquals(60, (int) exerciseDto.getRestPeriod());
    }
}