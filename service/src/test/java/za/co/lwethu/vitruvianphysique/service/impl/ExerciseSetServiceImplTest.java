package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import za.co.lwethu.vitruvianphysique.common.dtos.ExerciseSetDto;
import za.co.lwethu.vitruvianphysique.persistence.entities.ExerciseSet;
import za.co.lwethu.vitruvianphysique.persistence.repositories.ExerciseSetRepository;
import za.co.lwethu.vitruvianphysique.service.ExerciseSetService;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExerciseSetServiceImplTest {

    private ExerciseSetService exerciseSetService;

    @Before
    public void setUp() throws Exception {
        ExerciseSetRepository exerciseSetRepository = mock(ExerciseSetRepository.class);
        exerciseSetService = new ExerciseSetServiceImpl(exerciseSetRepository);

        when(exerciseSetRepository.save(any())).thenAnswer((Answer<ExerciseSet>) invocationOnMock -> (ExerciseSet)invocationOnMock.getArgument(0));
    }

    @Test
    public void saveExerciseSet() {
        ExerciseSetDto input = new ExerciseSetDto();
        input.setWeight(10.0);
        ExerciseSetDto exerciseSetDto = exerciseSetService.saveExerciseSet(input);
        assertEquals(10.0, exerciseSetDto.getWeight(), 0);
    }

}