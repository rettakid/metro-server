package za.co.lwethu.vitruvianphysique.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.springframework.data.jpa.domain.Specification;
import za.co.lwethu.vitruvianphysique.common.dtos.MuscleGroupDto;
import za.co.lwethu.vitruvianphysique.persistence.entities.MuscleGroup;
import za.co.lwethu.vitruvianphysique.persistence.repositories.MuscleGroupRepository;
import za.co.lwethu.vitruvianphysique.service.MuscleGroupService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MuscleGroupServiceImplTest {

    private MuscleGroupService muscleGroupService;

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        MuscleGroupRepository muscleGroupRepository = mock(MuscleGroupRepository.class);
        muscleGroupService = new MuscleGroupServiceImpl(muscleGroupRepository);

        MuscleGroup muscleGroup = new MuscleGroup();
        muscleGroup.setName("Chest");

        when(muscleGroupRepository.findAll(any(Specification.class))).thenReturn(Collections.singletonList(muscleGroup));
    }

    @Test
    public void getMuscleGroups() {
        List<MuscleGroupDto> muscleGroups = muscleGroupService.getMuscleGroups();
        assertEquals("Chest", muscleGroups.get(0).getName());
    }

}